import requests
import json

url = "http://oapi.5g-msg.com:31001/walnut/v1/accessToken"

payload = json.dumps({
  "appId": "687c31b0f67c11ec8d94e9948fdbe5ea",
  "nonce": "20201208013659abcdef",
  "signature": "67a32f920fcae5908ad25b1dd959605baa394471ab2795fd42aeb05d7c6be925"
})
headers = {
  'Content-Type': 'application/json'
}

response = requests.request("POST", url, headers=headers, data=payload)

token = json.loads(response.text)['data']['token']



url = "http://oapi.5g-msg.com:31001/walnut/v1/sendMessage"

payload = json.dumps({
  "contributionId": "cb1188a3-37ec-1037-9054-2dc66e443752",
  "conversationId": "cb1188a3-37ec-1037-9054-2dc66e443732",
  "clientCorrelator": "b1188a337",
  "messageType": "card",
  "destinationAddress": [
    "18814191265"
  ],
  "smsSupported": True,
  "storeSupported": False,
  "smsContent": "hello world!",
  "content": {
    "layout": {
      "cardOrientation": "VERTICAL"
    },
    "media": [
      {
        "mediaId": "62bc4f51e4b0210122c7ae98",
        "thumnailId": "62bc4f51e4b0210122c7ae99",
        "height": "MEDIUM_HEIGHT",
        "contentDescription": "检测到垃圾乱丢现象",
        "title": "检测到垃圾乱丢现象",
        "description": "地点：澄海区上华镇新园路一横23号",
        "suggestions": [
          {
            "type": "reply",
            "displayText": "远程喊话",
            "postbackData": "set_by_chatbot_reply_yes"
          },
          {
            "type": "mapAction",
            "displayText": "导航去现场",
            "postbackData": "set_by_chatbot_action_location",
            "actionParams": {
              "latitude": 23.5205352606698,
              "label": "测试打开地图",
              "url": "",
              "longitude": 116.71819288579594
            }
          },
          {
            "type": "dialerAction",
            "displayText": "联系网格员",
            "postbackData": "set_by_chatbot_reply_dialer",
            "actionParams": {
              "phoneNumber": "18814191265"
            }
          },
          {
            "type": "reply",
            "displayText": "误报忽略",
            "postbackData": "set_by_chatbot_reply_yes"
          }
        ]
      }
    ]
  }
})
headers = {
  'accessToken': token,
  'Content-Type': 'application/json'
}

response = requests.request("POST", url, headers=headers, data=payload)

print(response.text)
