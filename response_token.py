import requests
import json

url = "http://oapi.5g-msg.com:31001/walnut/v1/accessToken"

payload = json.dumps({
  "appId": "687c31b0f67c11ec8d94e9948fdbe5ea",
  "nonce": "20201208013659abcdef",
  "signature": "67a32f920fcae5908ad25b1dd959605baa394471ab2795fd42aeb05d7c6be925"
})
headers = {
  'Content-Type': 'application/json'
}

response = requests.request("POST", url, headers=headers, data=payload)

print(response.text)
