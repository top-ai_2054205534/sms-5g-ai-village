# -*- coding: utf-8 -*-
# code by xuxueyan 20220602
import io
import json
import sys

import requests

# sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf8')
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')


# 获取区域信息
def post_get_all_area():
    url = "http://36.155.105.73:8081/ts/api/area.do?method=getAllArea"
    # &tokenId=THIRDPART_20210513U1gte2&secretKey=KEY_uI0hwLNB
    data = {
        "tokenId": "THIRDPART_20210513U1gte2",
        "secretKey": "KEY_uI0hwLNB"
    }
    # post_data = json.dumps(data)
    result = requests.post(url, data=data)
    # logging.info(result.text.encode('utf8').decode('unicode_escape'))
    return result.text


# 获取所有终端信息
def post_get_all_term():
    url = "http://36.155.105.73:8081/ts/api/term.do?method=getAllTerm"
    data = {
        "tokenId": "THIRDPART_20210513U1gte2",
        "secretKey": "KEY_uI0hwLNB"
    }
    result = requests.post(url, data=data)
    return result.text


# 获取指定区域终端信息
# 全球通音柱areaCode=445704004001
def post_get_term_by_areaCode(areaCode):
    url = "http://36.155.105.73:8081/ts/api/term.do?method=getTermsByAreaCode"
    data = {
        "tokenId": "THIRDPART_20210513U1gte2",
        "secretKey": "KEY_uI0hwLNB",
        "areaCode": areaCode
    }
    result = requests.post(url, data=data)
    return result.text


# post_get_term_by_area_code(445704004001)

# 获取指定终端信息
def post_get_term(termeIds):
    url = "http://36.155.105.73:8081/ts/api/term.do?method=getTerms"
    data = {
        "tokenId": "THIRDPART_20210513U1gte2",
        "secretKey": "KEY_uI0hwLNB",
        "termeIds": termeIds
    }
    result = requests.post(url, data=data)
    return result.text


# post_get_term(4831)

# 上传文件
def post_upload_media_file(media_url):
    url = "http://36.155.105.73:8081/ts/api/media.do?method=uploadFile&tokenId=THIRDPART_20210513U1gte2&secretKey=KEY_uI0hwLNB"
    '''
    data = {
            "tokenId": "THIRDPART_20210513U1gte2",
            "secretKey": "KEY_uI0hwLNB"
        }
    '''
    files = {'file': open(media_url, 'rb')}
    result = requests.post(url, files=files)
    return result.text


# post_upload_media_file('E:\\产品研发中心\\云喇叭\\1002010.mp3')

# 获取媒体文件
def post_get_all_media_file(fileId):
    url = "http://36.155.105.73:8081/ts/api/media.do?method=getAllMediaFile"
    data = {
        "tokenId": "THIRDPART_20210513U1gte2",
        "secretKey": "KEY_uI0hwLNB",
        "fileId": fileId
    }
    result = requests.post(url, data=data)
    return result.text


# post_get_all_media_file(17430)

# 创建任务
def post_create_task():
    url = "http://36.155.105.73:8081/ts/api/task.do?method=createTask"
    taskjson = {"tname": "测试音频任务",
                "ttype": 1,  # 任务类型 1-音频任务 2-文本任务 4-推送任务#"ttextContent":"你好呀", #广播内容（文本任务时）
                "broadcaster": "xiaoyan",
                "tstartTime": "2021-05-14 11:38:00",
                "tdayOfWeekMask": 0,  # 任务周期  1-一次性任务 3-每周任务
                "tduration": 23700,
                "tkeeplive": 23700,
                "tvolume": 80,
                "tpriority": 0,
                "bcloop": 1,
                "playMode": 1,
                "pushType": 1,  # 推送类型：1-文件播放（推送任务生效）
                "mediaOut": 14,  # 输出媒体格式 14-MP3 15-AAC 255-保留原格式
                "bitrateOut": 64000}
    target = 445704004001
    fid = 17430  # 播放的文件ID 非文本任务时必填
    data = {
        "tokenId": "THIRDPART_20210514iHaLb3",
        "secretKey": "KEY_N3OXhHsN",
        "taskjson": taskjson,
        "target": target,
        "fid": fid
    }
    result = requests.post(url, data=data)
    return result.text


# post_create_task()


# 启动任务
def post_start_task(cloud_url, tokenId, secretKey, ids):
    url = cloud_url + "/task.do?method=startTask"
    data = {
        "tokenId": str(tokenId),
        "secretKey": str(secretKey),
        "ids": int(ids)
    }
    result = requests.post(url, data=data)
    return result.text


# 停止任务
def post_stop_task(cloud_url, tokenId, secretKey, ids):
    url = cloud_url + "/task.do?method=stopTask"
    data = {
        "tokenId": str(tokenId),
        "secretKey": str(secretKey),
        "ids": int(ids)
    }
    result = requests.post(url, data=data)
    return result.text


# 获取指定区域终端状态
# 终端状态：0-离线 1-未知 2-空闲 3-工作 4-故障
def post_get_terms_status_by_areaCode(cloud_url, tokenId, secretKey, areaCode):
    url = cloud_url + "/term.do?method=getTermsStatusByAreaCode"
    data = {
        "tokenId": str(tokenId),
        "secretKey": str(secretKey),
        "areaCode": areaCode
    }
    result = requests.post(url, data=data)
    return result.text


# res = post_get_terms_status_by_areaCode("http://36.155.105.73:8081/ts/api", "THIRDPART_20220429SuFFQ9",
#                                         "KEY_itceNBWg", "445704004001")
# print(res)


# 获取指定终端状态
# 终端状态：0-离线 1-未知 2-空闲 3-工作 4-故障
# 终端ID（对应tid值） 示例：10,11（多个以逗号分隔）
def post_get_terms_status_by_ids(cloud_url, tokenId, secretKey, termeIds):
    url = cloud_url + "/term.do?method=getTermsStatusByIds"
    data = {
        "tokenId": str(tokenId),
        "secretKey": str(secretKey),
        "termeIds": str(termeIds)
    }
    result = requests.post(url, data=data)
    return result.text


# res = post_get_terms_status_by_ids("http://36.155.105.73:8081/ts/api", "THIRDPART_20220429SuFFQ9",
#                                    "KEY_itceNBWg", "4831")
# print(res)


# 查询所有任务
def post_get_all_task():
    url = "http://36.155.105.73:8081/ts/api/task.do?method=getAllTask"
    data = {
        "tokenId": "THIRDPART_20220429SuFFQ9",  # THIRDPART_20210513U1gte2
        "secretKey": "KEY_itceNBWg",  # KEY_uI0hwLNB
        "id": "",  # id为空则查询所有时间段内任务
        "begin": "",
        "end": ""
    }
    result = requests.post(url, data=data)
    result.encoding = result.apparent_encoding
    return result.text

# id 73541 人员区域入侵
# id 79070 违章停车

# jict = eval(post_get_all_task())
# print(json.dumps(obj=jict, indent=4, ensure_ascii=False))
