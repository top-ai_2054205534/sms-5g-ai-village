import requests
import json

url = "http://oapi.5g-msg.com:31001/walnut/v1/accessToken"

payload = json.dumps({
  "appId": "687c31b0f67c11ec8d94e9948fdbe5ea",
  "nonce": "20201208013659abcdef",
  "signature": "67a32f920fcae5908ad25b1dd959605baa394471ab2795fd42aeb05d7c6be925"
})
headers = {
  'Content-Type': 'application/json'
}

response = requests.request("POST", url, headers=headers, data=payload)

token = json.loads(response.text)['data']['token']



url = "http://oapi.5g-msg.com:31001/walnut/v1/media/upload"

payload={}
files=[
  ('file',('b_爱奇艺.jpg',open('jgShUbdXh/b_爱奇艺.jpg','rb'),'image/jpeg')),
  ('thumbnail',('b_爱奇艺.jpg',open('VGFIXafxl/b_爱奇艺.jpg','rb'),'image/jpeg'))
]
headers = {
  'accessToken': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI4MTY2MzUyMzIyIiwiZXhwIjoxNjU2NTIyOTAzfQ.l9TPsDr_uUvWpSghLrXHJPzOB-oUMXqUi44xoMaF1Hg'
}

response = requests.request("POST", url, headers=headers, data=payload, files=files)

print(response.text)
